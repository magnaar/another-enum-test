'use strict'

import test from "ava"
import { Enum, EnumValue } from "../another-enum"

test("Create a new Enum", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(!!Colors, true)
    t.is(Object.keys(Colors).length, 3)
    t.is(Colors.RED == "RED", true)
    t.is(+Colors.RED == 0, true)
    t.is(Colors.GREEN == "GREEN", true)
    t.is(+Colors.GREEN == 1, true)
    t.is(Colors.BLUE == "BLUE", true)
    t.is(+Colors.BLUE == 2, true)
})

test("Enum values with special values", t => {
    const Time = Enum.Time({
        SECOND: 1,
        MINUTE: 60,
        HOUR: 3600
    })
    t.is(!!Time, true)
    t.is(Time.SECOND == "SECOND", true)
    t.is(+Time.SECOND == 1, true)
    t.is(Time.MINUTE == "MINUTE", true)
    t.is(+Time.MINUTE == 60, true)
    t.is(Time.HOUR == "HOUR", true)
    t.is(+Time.HOUR == 3600, true)
})

test("Weird test", t => {
    const Colors = Enum.Colors({
        RED: "#FF0000",
        GREEN: "#00FF00",
        BLUE: "#0000FF"
    })
    t.is(!!Colors, true)
    t.is(Colors.RED == "RED", true)
    t.is(Colors.RED.value == "#FF0000", true)
    t.is(Colors.GREEN == "GREEN", true)
    t.is(Colors.GREEN.value == "#00FF00", true)
    t.is(Colors.BLUE == "BLUE", true)
    t.is(Colors.BLUE.value == "#0000FF", true)
    t.is(Colors.BLUE.toString(), "Colors.BLUE(#0000FF)")
    t.is(Colors.get("#00FF00").longName, "Colors.GREEN")
})

test("Create a new Enum with options.base", t => {
    const Colors = Enum.Colors({ base: 16 }, "RED", "GREEN", "BLUE")
    t.is(Colors.RED.toString(), 'Colors.RED(16:0)')
    t.is(Colors.GREEN.toString(), 'Colors.GREEN(16:1)')
    t.is(Colors.BLUE.toString(), 'Colors.BLUE(16:2)')
})

test("Create a new Enum with options.base and special values", t => {
    const Colors = Enum.Colors({ base: 16 }, {
        "RED": 0xFF0000,
        "GREEN": 0x00FF00,
        "BLUE": 0x0000FF
    })
    t.is(Colors.RED.toString(), 'Colors.RED(16:FF0000)')
    t.is(Colors.GREEN.toString(), 'Colors.GREEN(16:00FF00)')
    t.is(Colors.BLUE.toString(), 'Colors.BLUE(16:0000FF)')
})

test("Create a new Enum with options.class", t => {
    class CustomEnumValue extends EnumValue {}
    const Colors = Enum.Colors({ class: CustomEnumValue }, "RED", "GREEN", "BLUE")
    t.is(Colors.RED.toString(), 'Colors.RED(0)')
    t.is(Colors.GREEN.toString(), 'Colors.GREEN(1)')
    t.is(Colors.BLUE.toString(), 'Colors.BLUE(2)')
})

test("Create a new Enum with options.base and options.class", t => {
    class CustomEnumValue extends EnumValue
    {
        toString()
        {
            return this.name
        }
    }
    const Colors = Enum.Colors({
        base: 16,
        class: CustomEnumValue
    }, "RED", "GREEN", "BLUE")
    t.is(Colors.RED.toString(), 'RED')
    t.is(Colors.GREEN.toString(), 'GREEN')
    t.is(Colors.BLUE.toString(), 'BLUE')
})

test("Create a new Enum with options.base, options.class and special values",
    t => {
        class CustomEnumValue extends EnumValue
        {
            toString()
            {
                return `|${this.index}+${super.toString()}|`
            }
        }
        const Colors = Enum.Colors({
                base: 16,
                class: CustomEnumValue
            }, {      
                "RED": 0xFF0000,
                "GREEN": 0x00FF00,
                "BLUE": 0x0000FF
            })
        t.is(Colors.RED.toString(), '|0+Colors.RED(16:FF0000)|')
        t.is(Colors.GREEN.toString(), '|1+Colors.GREEN(16:00FF00)|')
        t.is(Colors.BLUE.toString(), '|2+Colors.BLUE(16:0000FF)|')
    }
)

test("Create \"recursive\" Enum", t => {
    const MILLISECOND = 1
    const SECOND = 1000 * MILLISECOND
    const MINUTE = 60 * SECOND
    const HOUR = 60 * MINUTE
    const Time = Enum.Time({
        MILLISECOND,
        SECOND,
        MINUTE,
        HOUR
    })
    t.is(Time.MILLISECOND.name, "MILLISECOND")
    t.is(+Time.MILLISECOND, MILLISECOND)
    t.is(Time.SECOND.name, "SECOND")
    t.is(+Time.SECOND, SECOND)
    t.is(Time.MINUTE.name, "MINUTE")
    t.is(+Time.MINUTE, MINUTE)
    t.is(Time.HOUR.name, "HOUR")
    t.is(+Time.HOUR, HOUR)
})

test("Create \"recursive\" Enum 2", t => {
    const Time = Enum.Time({
        MILLISECOND: 1,
        SECOND: (e) => 1000 * e.MILLISECOND,
        MINUTE: (e) => 60 * e.SECOND,
        HOUR: (e) => 60 * e.MINUTE
    })
    t.is(Time.MILLISECOND.name, "MILLISECOND")
    t.is(+Time.MILLISECOND, 1)
    t.is(Time.SECOND.name, "SECOND")
    t.is(+Time.SECOND, 1000)
    t.is(Time.MINUTE.name, "MINUTE")
    t.is(+Time.MINUTE, 60000)
    t.is(Time.HOUR.name, "HOUR")
    t.is(+Time.HOUR, 3600000)
})

test("Auto increment value when using an empty object config constructor", t => {
    const Values = Enum.Values(/* Optional: EnumParameters, */ {
        VALUE1: {
            value: 2
        },
        VALUE2: {}, // value: 3, class: EnumValue
        VALUE3: {
            value: 8
        },
        VALUE4: {}, // value: 9, class: EnumValue
        VALUE5: {}, // value: 10, class: EnumValue
        /* ... */
    })
    t.is(+Values.VALUE1, 2)
    t.is(+Values.VALUE2, 3)
    t.is(+Values.VALUE3, 8)
    t.is(+Values.VALUE4, 9)
    t.is(+Values.VALUE5, 10)
})

test("Auto increment value when using Object litteral constructor", t => {
    const Values = Enum.Values(/* Optional: EnumParameters, */ {
        VALUE1: 2,
        VALUE2: {}, // value: 3, class: EnumValue
        VALUE3: 8,
        VALUE4: {}, // value: 9, class: EnumValue
        VALUE5: {}, // value: 10, class: EnumValue
        /* ... */
    })
    t.is(+Values.VALUE1, 2)
    t.is(+Values.VALUE2, 3)
    t.is(+Values.VALUE3, 8)
    t.is(+Values.VALUE4, 9)
    t.is(+Values.VALUE5, 10)
})
