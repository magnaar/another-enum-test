'use strict'

import test from "ava"
import { Enum, EnumValue } from "../another-enum"
import { hidden } from "../src/symbols"

/*test("Create a new Enum", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(!!Colors, true)
    t.is(Object.keys(Colors).length, 3)
    t.is(Colors.RED == "RED", true)
    t.is(+Colors.RED == 0, true)
    t.is(Colors.GREEN == "GREEN", true)
    t.is(+Colors.GREEN == 1, true)
    t.is(Colors.BLUE == "BLUE", true)
    t.is(+Colors.BLUE == 2, true)
})
*/
test("Enum values are of CustomEnumValue type", t => {
    class CustomEnumValue extends EnumValue {}
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    t.is(Values.VALUE_1 instanceof CustomEnumValue, true)
    t.is(Values.VALUE_2 instanceof CustomEnumValue, true)
})

test("Enum values have CustomEnumValue.method", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        method() {}
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
        t.is(!!value.method, true)
})

test("Enum values can call CustomEnumValue.method", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        method() { return this.name }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
        t.is(value.method(), value.name)
})

test("Enum values have CustomEnumValue.property", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        get property() { return "42" }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
        t.is(!!value.property, true)

})

test("Enum values can get CustomEnumValue.property", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        get property() { return "42" + this.value }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
        t.is(value.property, "42" + value.value)

})

test("Enum values can set CustomEnumValue.property", t => {
    t.plan(4)
    class CustomEnumValue extends EnumValue
    {
        init() { this._property }

        get property() { return this._property }
        set property(value) { this._property = value }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
    {
        t.is(value.property, undefined)
        value.property = value.name + value.value
        t.is(value.property, value.name + value.value)
    }
})

test("Enum values can't call CustomEnumValue static method", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        static method(value) { return value }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
        t.is(value.method, undefined)
})

test("Enum have CustomEnumValue.method", t => {
    class CustomEnumValue extends EnumValue
    {
        method() {}
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    t.is(!!Values.method, true)
})

test("Enum can call CustomEnumValue.method with EnumValue", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        method() { return this.name }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
        t.is(Values.method(value), value.name)
})

test("Enum have CustomEnumValue.property", t => {
    class CustomEnumValue extends EnumValue
    {
        get property() { return "42" }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    t.is(!!Values.property, true)

})

test("Enum can get CustomEnumValue.property with EnumValue", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        get property() { return "42" + this.value }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
        t.is(Values.property(value), "42" + value.value)

})

test("Enum can set CustomEnumValue.property with EnumValue", t => {
    t.plan(4)
    class CustomEnumValue extends EnumValue
    {
        init() { this._property }

        get property() { return this._property }
        set property(value) { this._property = value }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
    {
        t.is(Values.property(value), undefined)
        Values.property(value, value.name + value.value)
        t.is(Values.property(value), value.name + value.value)
    }
})

test("Enum can call CustomEnumValue.method with int value", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        method() { return this.name }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
        t.is(Values.method(value.value), value.name)
})

test("Enum can get CustomEnumValue.property with int value", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        get property() { return "42" + this.value }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
        t.is(Values.property(value.value), "42" + value.value)
})

test("Enum can set CustomEnumValue.property with int value", t => {
    t.plan(4)
    class CustomEnumValue extends EnumValue
    {
        init() { this._property }

        get property() { return this._property }
        set property(value) { this._property = value }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
    {
        t.is(Values.property(value), undefined)
        Values.property(value.value, value.name + value.value)
        t.is(Values.property(value), value.name + value.value)
    }
})

test("Enum can call CustomEnumValue.method with name", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        method() { return this.name }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
        t.is(Values.method(value.name), value.name)
})

test("Enum can get CustomEnumValue.property with name", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        get property() { return "42" + this.value }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
        t.is(Values.property(value.name), "42" + value.value)
})

test("Enum can set CustomEnumValue.property with name", t => {
    t.plan(4)
    class CustomEnumValue extends EnumValue
    {
        init() { this._property }

        get property() { return this._property }
        set property(value) { this._property = value }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
    {
        t.is(Values.property(value), undefined)
        Values.property(value.name, value.name + value.value)
        t.is(Values.property(value), value.name + value.value)
    }
})

test("Enum can call CustomEnumValue static method", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        static method(value) { return value }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
        t.is(Values.method(value), value)
})

test("Enum can call CustomEnumValue static method with parameters", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        static method(value) { return value * value }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    var i = 1
    for (const value of Values)
        t.is(Values.method(1 + +value), i * i++)
})

test("Enum can set CustomEnumValue.property with name (general init)", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        init(parent)
        {
            if (this === parent.VALUE_1)
                this._property = 'value 1'
            if (this === parent.VALUE_2)
                this._property = 'value 2'
        }

        get property() { return this._property }
    }
    const Values = Enum.Test(CustomEnumValue, 'VALUE_1', 'VALUE_2')
    for (const value of Values)
    {
        t.is(Values.property(value), 'value ' + (value.value + 1))
    }
})

test("Enum can set CustomEnumValue.property with name (specific init)", t => {
    t.plan(2)
    class CustomEnumValue extends EnumValue
    {
        get property() { return this._property }
    }
    class CustomEnumValue1 extends CustomEnumValue
    {
        init() { this._property = 'Hello' }
    }
    class CustomEnumValue2 extends CustomEnumValue
    {
        init() { this._property = 'World' }
    }
    const Values = Enum.Test(CustomEnumValue, {
        'VALUE_1': { class: CustomEnumValue1 },
        'VALUE_2': { class: CustomEnumValue2 }
    })
    const expected = ['Hello', 'World']
    for (const value of Values)
    {
        t.is(Values.property(value), expected[+value.value])
    }
})

test("Enum can't have EnumValue with the same name as a static member", t => {
    class CustomEnumValue extends EnumValue
    {
        static get VALUE_0() { return 42 }
    }

    try
    {
        const Values = Enum.Test(CustomEnumValue,
            'VALUE_1',
            'VALUE_2',
            'VALUE_0'
        )
        t.fail(`Enum can't have EnumValue with the same name as a static member`)
    }
    catch (err)
    {
        t.is(err.message, `Can't add the static member "VALUE_0" on Enum "Test",`
            + ` the name is already defined`)
    }
})

test("Enum can't have EnumValue with the same name as a member", t => {
    class CustomEnumValue extends EnumValue
    {
        get VALUE_0() { return 42 }
    }

    try
    {
        const Values = Enum.Test(CustomEnumValue,
            'VALUE_1',
            'VALUE_2',
            'VALUE_0'
        )
        t.fail(`Enum can't have EnumValue with the same name as a member`)
    }
    catch (err)
    {
        t.is(err.message, `Can't add the member "VALUE_0" on Enum "Test",`
            + ` the name is already defined`)
    }
})

test("Specific EnumValue can have specific method", t => {
    t.plan(4)
    class CustomEnumValue extends EnumValue
    {
        get property() { return 'Hello World' }
    }
    class CustomEnumValue1 extends CustomEnumValue
    {
        get answer() { return 42 }
    }
    const Values = Enum.Test(CustomEnumValue, {
        VALUE_1: { class: CustomEnumValue1 },
        VALUE_2: {}
    })
    const expected = [42, undefined]
    for (const value of Values)
    {
        t.is(value.property, 'Hello World')
        t.is(value.answer, expected[value.index])
    }
})

test("EnumValues have normal behavior if class is not specified", t => {
    t.plan(4)
    const Values = Enum.Test({
        VALUE_1: {},
        VALUE_2: {}
    })
    const expected = [
        {
            value: 0,
            tostring: 'Test.VALUE_1(0)'
        },
        {
            value: 1,
            tostring: 'Test.VALUE_2(1)'
        }
    ]
    for (const value of Values)
    {
        t.is(value.value, expected[value.index].value)
        t.is(value.toString(), expected[value.index].tostring)
    }
})

test("Enum can find custom EnumValue by modified values", t => {
    t.plan(4)
    class CustomEnumValue1 extends EnumValue
    {
        get value() { return 42 }
    }

    class CustomEnumValue2 extends EnumValue
    {
        get value() { return 1337 }
    }

    const Values = Enum.Test({
        VALUE_1: { class: CustomEnumValue1, value: 2 },
        VALUE_2: { class: CustomEnumValue2, value: 4 }
    })
    const expected = {
        2: undefined,
        4: undefined,
        42: Values.VALUE_1,
        1337: Values.VALUE_2
    }
    Object.keys(expected)
        .forEach(
            v => t.is(Values.get(v), expected[v])
        )
})

test("setting EnumValue's value (auto)", t => {
    class CustomEnumValue extends EnumValue
    {
    }
    const Values = Enum.Values({
        VALUE1: {},
        VALUE2: { class: CustomEnumValue }
    })

    t.is(+Values.VALUE2, 1)
})

test("setting EnumValue's value (init hidden set)", t => {
    class CustomEnumValue extends EnumValue
    {
        init() { this[hidden].value = 42 }
    }
    const Values = Enum.Values({
        VALUE1: {},
        VALUE2: { class: CustomEnumValue }
    })

    t.is(+Values.VALUE2, 42)
})

test("setting EnumValue's value (init override)", t => {
    class CustomEnumValue extends EnumValue
    {
        init() { this._value = 42 }
        get value() { return this._value }
    }
    const Values = Enum.Values({
        VALUE1: {},
        VALUE2: { class: CustomEnumValue }
    })

    t.is(+Values.VALUE2, 42)
})

test("setting EnumValue's value (init override can read super value)", t => {
    class CustomEnumValue extends EnumValue
    {
        init() { this._value = super.value }
        get value() { return this._value }
    }
    const Values = Enum.Values({
        VALUE1: {},
        VALUE2: { class: CustomEnumValue, value: 2 }
    })

    t.is(+Values.VALUE2, 2)
})

test("setting EnumValue's value (value override prevail)", t => {
    class CustomEnumValue extends EnumValue
    {
        get value() { return 42 }
    }
    const Values = Enum.Values({
        VALUE1: {},
        VALUE2: { class: CustomEnumValue, value: 2 }
    })

    t.is(+Values.VALUE2, 42)
})
