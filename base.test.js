'use strict'

import test from "ava"
import { Enum } from "../another-enum"

test("Base 16 Enum values toString", t => {
    const Colors = Enum.Colors(16, {
        RED: 0xFF0000,
        GREEN: 0x00FF00,
        BLUE: 0x0000FF
    })
    t.is(Colors.RED.toString(), "Colors.RED(16:FF0000)")
    t.is(Colors.GREEN.toString(), "Colors.GREEN(16:00FF00)")
    t.is(Colors.BLUE.toString(), "Colors.BLUE(16:0000FF)")
})

test("Base 16 string Enum values toString", t => {
    const Colors = Enum.Colors(16, {
        RED: 'FF0000',
        GREEN: '0x00FF00',
        BLUE: '0000FF'
    })
    t.is(Colors.RED.toString(), "Colors.RED(16:FF0000)")
    t.is(Colors.GREEN.toString(), "Colors.GREEN(16:00FF00)")
    t.is(Colors.BLUE.toString(), "Colors.BLUE(16:0000FF)")
})

test("Base 2 Enum values toString", t => {
    const Colors = Enum.Colors(2, {
        RED: 0b100,
        GREEN: 0b010,
        BLUE: 0b001
    })
    t.is(Colors.RED.toString(), "Colors.RED(2:100)")
    t.is(Colors.GREEN.toString(), "Colors.GREEN(2:010)")
    t.is(Colors.BLUE.toString(), "Colors.BLUE(2:001)")
})

test("Base 2 string Enum values toString", t => {
    const Colors = Enum.Colors(2, {
        RED: '100',
        GREEN: '010',
        BLUE: '001'
    })
    t.is(Colors.RED.toString(), "Colors.RED(2:100)")
    t.is(Colors.GREEN.toString(), "Colors.GREEN(2:010)")
    t.is(Colors.BLUE.toString(), "Colors.BLUE(2:001)")
})

test("Base 8 Enum values toString", t => {
    const Colors = Enum.Colors(8, {
        RED: 0o700,
        GREEN: 0o070,
        BLUE: 0o007
    })
    t.is(Colors.RED.toString(), "Colors.RED(8:700)")
    t.is(Colors.GREEN.toString(), "Colors.GREEN(8:070)")
    t.is(Colors.BLUE.toString(), "Colors.BLUE(8:007)")
})

test("Base 8 string Enum values toString", t => {
    const Colors = Enum.Colors(8, {
        RED: '700',
        GREEN: '070',
        BLUE: '007'
    })
    t.is(Colors.RED.toString(), "Colors.RED(8:700)")
    t.is(Colors.GREEN.toString(), "Colors.GREEN(8:070)")
    t.is(Colors.BLUE.toString(), "Colors.BLUE(8:007)")
})

test("Base 3 string Enum values toString", t => {
    const Colors = Enum.Colors(3, {
        RED: '200',
        GREEN: '020',
        BLUE: '002'
    })
    t.is(Colors.RED.toString(), "Colors.RED(3:200)")
    t.is(Colors.GREEN.toString(), "Colors.GREEN(3:020)")
    t.is(Colors.BLUE.toString(), "Colors.BLUE(3:002)")
})

test("Base 20 string Enum values toString", t => {
    const Colors = Enum.Colors(20, {
        RED: 'J00',
        GREEN: '0J0',
        BLUE: '00J'
    })
    t.is(Colors.RED.toString(), "Colors.RED(20:J00)")
    t.is(Colors.GREEN.toString(), "Colors.GREEN(20:0J0)")
    t.is(Colors.BLUE.toString(), "Colors.BLUE(20:00J)")
})

test("Base 16 Enum values toString full int", t => {
    const Colors = Enum.Colors(16, {
        RED: 0xFF0000FF,
        GREEN: 0x00FF00FF,
        BLUE: 0x0000FFFF
    })
    t.is(Colors.RED.toString(), "Colors.RED(16:FF0000FF)")
    t.is(Colors.GREEN.toString(), "Colors.GREEN(16:00FF00FF)")
    t.is(Colors.BLUE.toString(), "Colors.BLUE(16:0000FFFF)")
})

test("Base 16 Enum values toString full int", t => {
    const Colors = Enum.Colors(16, {
        RED: 0xFF0000FF,
        GREEN: 0x00FF00FF,
        BLUE: 0x0000FFFF
    })
    t.is(Colors.RED.toString(), "Colors.RED(16:FF0000FF)")
    t.is(Colors.GREEN.toString(), "Colors.GREEN(16:00FF00FF)")
    t.is(Colors.BLUE.toString(), "Colors.BLUE(16:0000FFFF)")
})
