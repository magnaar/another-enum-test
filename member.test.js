'use strict'

import test from "ava"
import { Enum, EnumValue } from "../another-enum"

test("Get Enum length", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(Colors.length, 3)
})

test("Get an Enum value long name", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(Colors.GREEN.longName, "Colors.GREEN")
})

test("Get an Enum index", t => {
    const Time = Enum.Time({
        SECOND: 1,
        MINUTE: 60,
        HOUR: 3600
    })
    t.is(Time.HOUR.index, 2)
})

test("Get an Enum value with its value", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(Colors.get(1).name, "GREEN")
})

test("Get an Enum value with its index", t => {
    const Time = Enum.Time({
        SECOND: 1,
        MINUTE: 60,
        HOUR: 3600
    })
    t.is(Time.getAt(2).name, "HOUR")
})

test("Enum value to int", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(+Colors.BLUE, 2)
    t.is(Colors.BLUE.value, 2)
})

test("EnumValue.stringValue doesn't throw", t => {
    class CustomEnumValue extends EnumValue
    {
        init()
        {
            this.getter = () => super.value
        }
        get value() { return this.getter() }
    }
    const Test = Enum.Test(CustomEnumValue, 'A', 'B')
    Test.A.getter = function () { throw new Error('HelloWorld!') }
    t.is(Test.A.toString(), 'Test.A(Error: HelloWorld!)')
})
