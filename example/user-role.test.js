'use strict'

import test from "ava"
import { Enum } from "../../another-enum"

test("Example UserRole: stringValue after bitshifting", t => {
    const UserRole = Enum
        .UserRole(16, {
            USER: 0x1,
            MODERATOR: (e) => 0x2 | e.USER,
            ADMIN: (e) => 0x4 | e.USER | e.MODERATOR,
            OWNER: (e) => 0x8 | e.USER | e.MODERATOR | e.ADMIN,
            SUPER_MODERATOR: (e) => e.MODERATOR << 4 | e.MODERATOR,
            SUPER_ADMIN: (e) => e.ADMIN << 4 | e.ADMIN,
            SUPER_OWNER: (e) => e.OWNER << 4 | e.OWNER
        })
    t.is(UserRole.USER.stringValue, '01')
    t.is(UserRole.MODERATOR.stringValue, '03')
    t.is(UserRole.ADMIN.stringValue, '07')
    t.is(UserRole.OWNER.stringValue, '0F')
    t.is(UserRole.SUPER_MODERATOR.stringValue, '33')
    t.is(UserRole.SUPER_ADMIN.stringValue, '77')
    t.is(UserRole.SUPER_OWNER.stringValue, 'FF')
})
