'use strict'

import test from "ava"
import { Enum, EnumValue } from "../../another-enum"

test("Example DateBoundary: static NOW", t => {
    class DateBoundaryEnumValue extends EnumValue
    {
        static get NOW()
        {
            return Date.now()
        }
    }

    const DateBoundary = Enum
        .DateBoundary(DateBoundaryEnumValue, {
            MIN: new Date(-8640000000000000).getTime(),
            MAX: new Date(8640000000000000).getTime()
        })
    
    t.true(+DateBoundary.NOW <= Date.now())
    t.is([...DateBoundary].length, 2)
})

test("Example DateBoundary: EnumValue NOW", t => {
    class DateBoundaryEnumValue extends EnumValue
    {
    }

    class NowEnumValue extends DateBoundaryEnumValue
    {
        get value()
        {
            return Date.now()
        }

        get stringValue()
        {
            return new Date(this.value)
        }
    }

    const DateBoundary = Enum
        .DateBoundary(DateBoundaryEnumValue, {
            MIN: new Date(-8640000000000000).getTime(),
            NOW: { class: NowEnumValue },
            MAX: new Date(8640000000000000).getTime()
        })
    
    t.true(+DateBoundary.NOW <= Date.now())
    t.is(DateBoundary.NOW.stringValue.toDateString(), new Date(Date.now()).toDateString())
    t.is([...DateBoundary][1], DateBoundary.NOW)
    t.is(+DateBoundary.MIN, new Date(-8640000000000000).getTime())
    t.is(+DateBoundary.MAX, new Date(8640000000000000).getTime())
})
