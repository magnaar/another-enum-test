'use strict'

import test from "ava"
import { Enum } from "../../another-enum"

test("Example UserRight: stringValue after bitshifting", t => {
    const bs = 12
    const UserRight = Enum
        .UserRight(16, {
            NONE: 0x0,

            SHOW_CONFIG: 0x001,
            CONFIGURE: 0x002,
            VACCUM: 0x004,
            SET_MODERATOR: 0x008,
            SET_ADMIN: 0x010,
            SEE_ROLES: 0x020,
            SEE_ROLE_RIGHTS: 0x040,
            SEE_USER_ROLES: 0x080,
            SEE_USER_RIGHTS: 0x100,

            CHANGE_DEFAULT_CONFIG: (e) => e.SHOW_CONFIG << bs,
            CONFIGURE_ALL: (e) => e.CONFIGURE << bs | e.CONFIGURE,
            VACCUM_ALL: (e) => e.VACCUM << bs | e.VACCUM,
            SET_SUPER_MODERATOR: (e) => e.SET_MODERATOR << bs | e.SET_MODERATOR,
            SET_SUPER_ADMIN: (e) => e.SET_ADMIN << bs | e.SET_ADMIN,
            SEE_SUPER_ROLES: (e) => e.SEE_ROLES << bs | e.SEE_ROLES,
            SEE_SUPER_ROLE_RIGHTS: (e) => e.SEE_ROLE_RIGHTS << bs | e.SEE_ROLE_RIGHTS,
            SEE_SUPER_USER_ROLES: (e) => e.SEE_USER_ROLES << bs | e.SEE_USER_ROLES,
            SEE_SUPER_USER_RIGHTS: (e) => e.SEE_USER_RIGHTS << bs | e.SEE_USER_RIGHTS,
        })

    t.is(UserRight.NONE.stringValue, '000000')

    t.is(UserRight.SHOW_CONFIG.stringValue, '000001')
    t.is(UserRight.CONFIGURE.stringValue, '000002')
    t.is(UserRight.VACCUM.stringValue, '000004')
    t.is(UserRight.SET_MODERATOR.stringValue, '000008')
    t.is(UserRight.SET_ADMIN.stringValue, '000010')
    t.is(UserRight.SEE_ROLES.stringValue, '000020')
    t.is(UserRight.SEE_ROLE_RIGHTS.stringValue, '000040')
    t.is(UserRight.SEE_USER_ROLES.stringValue, '000080')
    t.is(UserRight.SEE_USER_RIGHTS.stringValue, '000100')

    t.is(UserRight.CHANGE_DEFAULT_CONFIG.stringValue, '001000')
    t.is(UserRight.CONFIGURE_ALL.stringValue, '002002')
    t.is(UserRight.VACCUM_ALL.stringValue, '004004')
    t.is(UserRight.SET_SUPER_MODERATOR.stringValue, '008008')
    t.is(UserRight.SET_SUPER_ADMIN.stringValue, '010010')
    t.is(UserRight.SEE_SUPER_ROLES.stringValue, '020020')
    t.is(UserRight.SEE_SUPER_ROLE_RIGHTS.stringValue, '040040')
    t.is(UserRight.SEE_SUPER_USER_ROLES.stringValue, '080080')
    t.is(UserRight.SEE_SUPER_USER_RIGHTS.stringValue, '100100')
})
