'use strict'

import test from "ava"
import { Enum, EnumValue } from "../../another-enum"

test("Example TimeUnit: static methods", t => {
    class TimeUnitEnumValue extends EnumValue
    {
        init(parent)
        {
            if (! TimeUnitEnumValue.reversedUnits)
                TimeUnitEnumValue.reversedUnits = Object.values(parent).reverse()
        }

        static splitMsAmount(msAmount)
        {
            const amounts = {}
            for (const unit of TimeUnitEnumValue.reversedUnits)
            {
                amounts[unit] = (msAmount / unit) | 0
                msAmount = msAmount % unit
            }
            return amounts
        }

        static getMsAmountFromUnit(text)
        {
            text = text.name || text.toUpperCase()
            for (const unit in TimeUnit)
                if (text == unit || text == unit + 'S')
                    return +TimeUnit[unit]
            return null
        }
    }

    const TimeUnit = Enum
        .TimeUnit(TimeUnitEnumValue, {
            'MINUTE': 1000 * 60,
            'HOUR': 1000 * 3600,
            'DAY': 1000 * 3600 * 24,
            'WEEK': 1000 * 3600 * 24 * 7,
            'MONTH': 1000 * 3600 * 24 * 30,
            'YEAR': 1000 * 3600 * 24 * 30 * 365
        })
    
    t.deepEqual(TimeUnit.splitMsAmount(123456000), { YEAR: 0, MONTH: 0, WEEK: 0, DAY: 1, HOUR: 10, MINUTE: 17 })
    t.deepEqual(TimeUnit.getMsAmountFromUnit("WEEKS"), 1000 * 60 * 60 * 24 * 7)
})
