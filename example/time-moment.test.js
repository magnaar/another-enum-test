'use strict'

import test from "ava"
import { Enum, EnumValue } from "../../another-enum"

test("Example TimeMoment: methods", t => {
    function removeTime(time)
    {
        time.setHours(0)
        time.setMinutes(0)
        time.setSeconds(0)
        time.setMilliseconds(0)
        return time
    }

    function addDay(time, days)
    {
        time.setTime(time.getTime() + days * TimeUnit.DAY)
        return time
    }

    class TimeMomentEnumValue extends EnumValue
    {
        init(parent)
        {
            TimeMomentEnumValue.timeGetters
                = TimeMomentEnumValue.timeGetters
                    || {
                        [parent.NOW]: () => new Date(Date.now()),
                        [parent.TODAY]: () => removeTime(new Date(Date.now())),
                        [parent.YERSTERDAY]: () => removeTime(addDay(new Date(Date.now()), -1)),
                        [parent.TOMORROW]: () => removeTime(addDay(new Date(Date.now()), 1)),
                        [parent.MIDDAY]: () => addDay(removeTime(new Date(Date.now())), 0.5),
                        [parent.MIDNIGHT]: () => addDay(removeTime(new Date(Date.now())), 1)
                    }
        }

        getTime()
        {
            return TimeMomentEnumValue.timeGetters[this.name.toUpperCase()]()
        }
    }

    const TimeMoment = Enum
        .TimeMoment(TimeMomentEnumValue,
            'NOW',
            'TODAY',
            'YERSTERDAY',
            'TOMORROW',
            'MIDDAY',
            'MIDNIGHT'
        )

    const time = TimeMoment.TODAY.getTime()
    const expected = removeTime(new Date(Date.now()))
    t.is(TimeMoment.TODAY.getTime().toString(), removeTime(new Date(Date.now())).toString())
})
