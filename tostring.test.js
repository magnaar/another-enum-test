'use strict'

import test from "ava"
import { Enum, EnumValue } from "../another-enum"

test("Stringify Enum", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(Colors.toString(), "Colors(RED|GREEN|BLUE)")
})

test("Stringify Enum with toStringTag", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(Object.prototype.toString.call(Colors), "[object Colors(RED|GREEN|BLUE)]")
})

test("Stringify Enum value by Enum", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(Colors.toString(Colors.GREEN), "Colors[GREEN](1)")
})

test("Stringify value by Enum", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(Colors.toString(1), "Colors[GREEN](1)")
})

test("Stringify nothing by Enum", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(Colors.toString(4), undefined)
})

test("Stringify Enum value", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(Colors.RED.toString(), "Colors.RED(0)")
    t.is(Colors.GREEN.toString(), "Colors.GREEN(1)")
    t.is(Colors.BLUE.toString(), "Colors.BLUE(2)")
})

test("Stringify Enum value with toStringTag", t => {
    const Colors = Enum.Colors("RED", "GREEN", "BLUE")
    t.is(Object.prototype.toString.call(Colors.RED), "[object Colors.RED(0)]")
    t.is(Object.prototype.toString.call(Colors.GREEN), "[object Colors.GREEN(1)]")
    t.is(Object.prototype.toString.call(Colors.BLUE), "[object Colors.BLUE(2)]")
})

test("Stringify composed bitmask", t => {
    const Colors = Enum.Colors(16, {
        RED: 0xFF0000,
        GREEN: 0x00FF00,
        BLUE: 0x0000FF
    })
    t.is(Colors.toString(Colors.RED | Colors.GREEN), "Colors[RED|GREEN](16:FFFF00)")
})

test("Stringify composed bitmask value", t => {
    const Colors = Enum.Colors(16, {
        RED: 0xFF0000,
        GREEN: 0x00FF00,
        BLUE: 0x0000FF
    })
    t.is(Colors.toString(0x00FFFF), "Colors[GREEN|BLUE](16:00FFFF)")
})

test("toString() is 0 padded even with value modify by init", t => {
    class ColorsEnumValue extends EnumValue
    {
        init(e)
        {
            switch (this)
            {
                case e.RED:
                    this._value = 0xFF0000
                    break
                case e.GREEN:
                    this._value = 0x00FF00
                    break
                case e.BLUE:
                    this._value = 0x0000FF
                    break
            }
        }
        get value() { return this._value }
    }
    const Colors = Enum.Colors({
        base: 16,
        class: ColorsEnumValue
    }, "RED", "GREEN", "BLUE")
    t.is(Object.prototype.toString.call(Colors.RED), '[object Colors.RED(16:FF0000)]')
    t.is(Object.prototype.toString.call(Colors.GREEN), '[object Colors.GREEN(16:00FF00)]')
    t.is(Object.prototype.toString.call(Colors.BLUE), '[object Colors.BLUE(16:0000FF)]')
})
